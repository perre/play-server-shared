package shared.message;

import junit.framework.TestCase;
import shared.config.DeviceInfo;

import java.util.Calendar;
import java.util.Date;
import java.util.Map;

public class SessionConditionTest extends TestCase {


    public void testIsTimeToShow() {
        final String name = "test";
        final SessionCondition sessionCondition = new SessionCondition(name, 5, 12);

        Date now = Calendar.getInstance().getTime();
        assertFalse(sessionCondition.isTimeToShow(now, TestHelper.createDeviceInfo(4), TestHelper.createSessionMetaData(name, 0)));
        assertFalse(sessionCondition.isTimeToShow(now, TestHelper.createDeviceInfo(5), TestHelper.createSessionMetaData(name, 0)));
        assertTrue(sessionCondition.isTimeToShow(now, TestHelper.createDeviceInfo(6), TestHelper.createSessionMetaData(name, 0)));

        assertFalse(sessionCondition.isTimeToShow(now, TestHelper.createDeviceInfo(99), TestHelper.createSessionMetaData(name, 100)));
        assertTrue(sessionCondition.isTimeToShow(now, TestHelper.createDeviceInfo(100), TestHelper.createSessionMetaData(name, 100)));
    }

    public void testUpdateTimeToShow() {
        final String name = "test";
        final int delteSessionId = 12;
        final SessionCondition sessionCondition = new SessionCondition(name, 5, delteSessionId);

        final Date now = Calendar.getInstance().getTime();
        final DeviceInfo deviceInfo = TestHelper.createDeviceInfo(100);
        final Map<String, String> metaData = TestHelper.createSessionMetaData(name, 100);

        assertTrue(sessionCondition.isTimeToShow(now, deviceInfo, metaData));

        sessionCondition.updateNextTimeToShow(now, deviceInfo, metaData);

        final String nextValue = metaData.get(name + ".sessionid");
        assertEquals("112", nextValue);
    }

    public void testUpdateTimeToShow2() {
        final String name = "test";
        final int delteSessionId = 12;
        final SessionCondition sessionCondition = new SessionCondition(name, 5, delteSessionId);

        final Date now = Calendar.getInstance().getTime();
        final DeviceInfo deviceInfo = TestHelper.createDeviceInfo(108);
        final Map<String, String> metaData = TestHelper.createSessionMetaData(name, 100);

        assertTrue(sessionCondition.isTimeToShow(now, deviceInfo, metaData));

        sessionCondition.updateNextTimeToShow(now, deviceInfo, metaData);

        final String nextValue = metaData.get(name + ".sessionid");
        assertEquals("120", nextValue);
    }
}