package shared.tracking;

import com.fasterxml.jackson.databind.JsonNode;

import java.util.Map;

public interface JsonHandler {
    void handle(String ip, JsonNode json, Map<String, Object> config);
}
