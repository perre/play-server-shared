package shared.theme.conditions;

import shared.config.DeviceInfo;

public interface DeviceInfoSelector {

    String select(DeviceInfo deviceInfo);

}
