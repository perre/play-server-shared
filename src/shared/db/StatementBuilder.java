package shared.db;

import java.sql.PreparedStatement;
import java.sql.SQLException;

public interface StatementBuilder {
    PreparedStatement getStatement() throws SQLException;
}
