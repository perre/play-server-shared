package shared.theme;

class ThemeConditionPair<THEME> {

    ThemeConditionPair(ThemeCondition themeCondition, THEME theme) {
        this.themeCondition = themeCondition;
        this.theme = theme;
    }

    final ThemeCondition themeCondition;
    final THEME theme;
}

