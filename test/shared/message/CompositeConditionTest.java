package shared.message;

import junit.framework.TestCase;
import shared.config.DeviceInfo;

import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

public class CompositeConditionTest extends TestCase {

    public void testIsTime() {
        final AtomicInteger updateCountTrue = new AtomicInteger(0);
        final AtomicInteger updateCountFalse = new AtomicInteger(0);

        MessageCondition conditionTrue = new MessageCondition() {
            @Override
            public boolean isTimeToShow(Date now, DeviceInfo deviceInfo, Map<String, String> messageDataSet) { return true; }

            @Override
            public void updateNextTimeToShow(Date now, DeviceInfo deviceInfo,  Map<String, String> messageDataSet) {
                updateCountTrue.addAndGet(1);
            }
        };
        MessageCondition conditionFalse = new MessageCondition() {
            @Override
            public boolean isTimeToShow(Date now, DeviceInfo deviceInfo, Map<String, String> messageDataSet) { return false; }

            @Override
            public void updateNextTimeToShow(Date now, DeviceInfo deviceInfo, Map<String, String> messageDataSet) {
                updateCountFalse.addAndGet(1);
            }
        };


        Date now = Calendar.getInstance().getTime();
        {
            CompositeCondition compositeCondition = new CompositeCondition(conditionFalse, conditionFalse, conditionFalse, conditionFalse);
            assertFalse(compositeCondition.isTimeToShow(now, TestHelper.createDeviceInfo(100), TestHelper.createSessionMetaData("test", 100)));
            assertEquals(0, updateCountTrue.get());
            assertEquals(0, updateCountFalse.get());
        }
        {
            CompositeCondition compositeCondition = new CompositeCondition();
            assertFalse(compositeCondition.isTimeToShow(now, TestHelper.createDeviceInfo(100), TestHelper.createSessionMetaData("test", 100)));
            assertEquals(0, updateCountTrue.get());
            assertEquals(0, updateCountFalse.get());
        }
        {
            CompositeCondition compositeCondition = new CompositeCondition(conditionFalse, conditionFalse, conditionTrue, conditionFalse);
            assertTrue(compositeCondition.isTimeToShow(now, TestHelper.createDeviceInfo(100), TestHelper.createSessionMetaData("test", 100)));
            compositeCondition.updateNextTimeToShow(now, TestHelper.createDeviceInfo(100), TestHelper.createSessionMetaData("test", 100));
            assertEquals(1, updateCountTrue.get());
            assertEquals(3, updateCountFalse.get());
        }


    }

}