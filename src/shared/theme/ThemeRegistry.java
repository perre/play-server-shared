package shared.theme;

import shared.config.DeviceInfo;

import java.util.Date;

public interface ThemeRegistry<THEME> {

    THEME getActiveTheme(Date now, DeviceInfo deviceInfo);

}
