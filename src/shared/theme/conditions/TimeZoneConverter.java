package shared.theme.conditions;

import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

class TimeZoneConverter {

    static String convert(String iosTimeZone) {
        String javaTimeZone = iosTimeZone;
        if (iosTimeZone != null) {
            iosTimeZone = iosTimeZone.replace('−', '-');
            if (iosTimeZone.startsWith("UTC")) {
                javaTimeZone = "GMT" + iosTimeZone.substring(3);
            } else {
                String betterForm = timeZoneMap.get(iosTimeZone);
                if (betterForm != null) {
                    javaTimeZone = betterForm;
                }
            }

        }
        return javaTimeZone;
    }

    private static String toGmtString(int offset) {
        StringWriter sw = new StringWriter();
        sw.append("GMT");
        if (offset >= 0) {
            sw.append(String.format("+%02d", offset));
        } else {
            sw.append(String.format("-%02d", -offset));
        }
        return sw.toString();
    }

    static private Map<String, String> populateTimeZoneMap() {
        final Map<String, String> map = new HashMap<String, String>();
        map.put("ADT",  toGmtString(-3));
        map.put("AST",  toGmtString(-4));
        map.put("AEDT", toGmtString(+11));
        map.put("AEST", toGmtString(+10));
        map.put("AKST", toGmtString(-9));
        map.put("BRST", toGmtString(-2));
        map.put("BST",  toGmtString(+1));
        map.put("CDT", 	toGmtString(-5));
        map.put("CET", 	toGmtString(+1));
        map.put("CEST", toGmtString(+2));
        map.put("EDT", 	toGmtString(-4));
        map.put("EET", 	toGmtString(+2));
        map.put("EEST", toGmtString(+3));
        map.put("EST",  toGmtString(-5));
        map.put("MDT",  toGmtString(-6));
        map.put("MESZ", toGmtString(+2));
        map.put("MEZ",  toGmtString(+1));
        map.put("PDT",  toGmtString(-7));
        map.put("SAST", toGmtString(+2));
        map.put("WITA", toGmtString(+8));
        return map;
    }

    final static private Map<String, String> timeZoneMap = populateTimeZoneMap();
}
