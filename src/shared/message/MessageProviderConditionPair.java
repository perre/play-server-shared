package shared.message;

public class MessageProviderConditionPair {

    public MessageProviderConditionPair(MessageCondition messageCondition, MessageProvider messageProvider) {

        this.messageCondition = messageCondition;
        this.messageProvider = messageProvider;
    }

    public final MessageCondition messageCondition;
    public final MessageProvider messageProvider;
}
