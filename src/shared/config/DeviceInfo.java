package shared.config;

public class DeviceInfo {

    public DeviceInfo(String ip, String deviceId, int sessionId, String version,
                      String systemName, String systemVersion, String deviceModel, String deviceName,
                      String timeZone, String language, String SSID, String BSSID, String sound) {
        this.ip = ip;
        this.deviceId = deviceId;
        this.sessionId = sessionId;
        this.version = version;
        this.systemName = systemName;
        this.systemVersion = systemVersion;
        this.deviceModel = deviceModel;
        this.deviceName = deviceName;
        this.timeZone = timeZone;
        this.language = language;
        this.SSID = SSID;
        this.BSSID = BSSID;
        this.sound = sound;
    }

    public final String ip;
    public final String deviceId;
    public final int sessionId;
    public final String version;
    public final String systemName;
    public final String systemVersion;
    public final String deviceModel;
    public final String deviceName;
    public final String timeZone;
    public final String language;
    public final String SSID;
    public final String BSSID;
    public final String sound;
}
