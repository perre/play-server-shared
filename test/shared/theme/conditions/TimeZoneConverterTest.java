package shared.theme.conditions;

import junit.framework.TestCase;

public class TimeZoneConverterTest extends TestCase {

    public void testUtc() {
        assertEquals("GMT+1", TimeZoneConverter.convert("UTC+1"));
        assertEquals("GMT", TimeZoneConverter.convert("UTC"));
        assertEquals("GMT-1", TimeZoneConverter.convert("UTC-1"));
    }

    public void testWeirdMinus() {
        final String weirdString = "UTC−4";
        assertEquals("GMT-4", TimeZoneConverter.convert(weirdString));

    }

    public void testOthers() {
        assertEquals("GMT-03", TimeZoneConverter.convert("ADT"));
    }
}