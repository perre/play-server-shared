package shared.tracking;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.Results;
import shared.playhelper.RequestParser;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class TrackingHelper {

    public Result process(Http.Request request) {

        boolean ok = false;
        final Map<String, Object> config = new HashMap<String, Object>();

        String ip = RequestParser.ipFromRequest(request);
        final Http.RequestBody body = request.body();
        if (body != null) {
            final JsonNode json = body.asJson();
            if (json != null) {
                for (JsonHandler handler : handlers) {
                    handler.handle(ip, json, config);
                }
                ok = true;
            }
        }

        if (ok) {
            if (config.size() > 0) {
                final String jsonResponse;
                try {
                    jsonResponse = mapper.writeValueAsString(config);
                    return Results.ok(jsonResponse);
                } catch (JsonProcessingException e) {
                }
            }
            return Results.noContent();
        }
        return Results.badRequest();
    }

    public void addHandler(JsonHandler handler) {
        handlers.add(handler);
    }

    private final List<JsonHandler> handlers = new LinkedList<JsonHandler>();

    private static final ObjectMapper mapper = new ObjectMapper();

}
