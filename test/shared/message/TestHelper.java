package shared.message;

import shared.config.DeviceInfo;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

class TestHelper {
    static DeviceInfo createDeviceInfo(int sessionId) {
        return new DeviceInfo("1.1.1.1", "abc-123", sessionId, "1.0", "test", "1.0", "testModel", "testDevice", "GMT+0", "en", "sparcnet", "1111111", "");
    }

    static Map<String, String> createSessionMetaData(String name, int sessionId) {
        final Map<String, String> metaData = new HashMap<String, String>();
        if (sessionId > 0) {
            metaData.put(name + ".sessionid", "" + sessionId);
        }
        return metaData;
    }

    static Map<String, String> createDateMetaData(String name, Date date) {
        final Map<String, String> metaData = new HashMap<String, String>();
        if (date != null) {
            metaData.put(name + ".date", TimeHelper.timeToString(date));
        }
        return metaData;
    }

}
