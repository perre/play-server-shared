package shared.push;

public interface PushDelegate {
    boolean isValidSource(String ip);
}
