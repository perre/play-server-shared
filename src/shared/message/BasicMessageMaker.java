package shared.message;

import shared.config.DeviceInfo;

import java.util.*;

public class BasicMessageMaker implements MessageMaker {

    public BasicMessageMaker(List<MessageProviderConditionPair> messageProviders, MetaDataStore metaDataStore) {
        this.providers = messageProviders;
        this.metaDataStore = metaDataStore;
    }

    @Override
    public MessageData[] getLaunchMessages(DeviceInfo deviceInfo) {
        Date now = Calendar.getInstance().getTime();
        final Map<String, String> metaDataSet = metaDataStore.getMetaData(deviceInfo.deviceId);
        final List<MessageData> messageDataList = new ArrayList<MessageData>();
        boolean updateMetaData = false;
        for (MessageProviderConditionPair messageProviderPair : providers) {
            final MessageCondition condition = messageProviderPair.messageCondition;
            final MessageProvider provider = messageProviderPair.messageProvider;
            if (condition.isTimeToShow(now, deviceInfo, metaDataSet)) {
                final MessageData messageData = provider.createMessage(deviceInfo);
                if (messageData != null) {
                    messageDataList.add(messageData);
                    condition.updateNextTimeToShow(now, deviceInfo, metaDataSet);
                    updateMetaData = true;
                }
            }
        }
        if (updateMetaData) {
            metaDataStore.putMetaData(deviceInfo.deviceId, metaDataSet);
        }
        if (messageDataList.size() > 0) {
            final MessageData[] messageDataArray = new MessageData[messageDataList.size()];
            return messageDataList.toArray(messageDataArray);
        }
        return null;
    }

    private List<MessageProviderConditionPair> providers;
    private final MetaDataStore metaDataStore;
}
