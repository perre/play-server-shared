package shared.message;

import java.util.Map;

public interface MetaDataStore {
    Map<String, String> getMetaData(String deviceId);
    void putMetaData(String deviceId, Map<String, String> metaData);
}
