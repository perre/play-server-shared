package shared.config;

import com.fasterxml.jackson.databind.JsonNode;

import java.util.Map;

public interface ConfigDataSource {

    boolean doTrack(DeviceInfo deviceInfo);
    boolean doPush(DeviceInfo deviceInfo);

    void updateConfig(Map<String, Object> config, DeviceInfo deviceInfo, JsonNode jsonNode);

    byte[] getMetaKey(String deviceId);
}
