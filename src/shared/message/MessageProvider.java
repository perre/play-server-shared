package shared.message;

import shared.config.DeviceInfo;

public interface MessageProvider {

    MessageData createMessage(DeviceInfo deviceInfo);
}
