package shared.message;

import junit.framework.TestCase;
import shared.config.DeviceInfo;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

public class DateConditionTest extends TestCase {

    public void testIsTimeToShow() throws ParseException {
        final String name = "test";
        final DateCondition dateCondition = new DateCondition(name, 12*3600);

        final Date now = TimeHelper.timeFromString("2016.02.15 10:32");

        final Calendar cal = Calendar.getInstance();
        cal.setTime(now);

        cal.add(Calendar.SECOND, 3600 * 12);
        final Date justBefore = cal.getTime();

        cal.add(Calendar.MINUTE, 1);
        final Date justAfter = cal.getTime();

        assertTrue(dateCondition.isTimeToShow(now, TestHelper.createDeviceInfo(1000), TestHelper.createDateMetaData(name, null)));

        assertFalse(dateCondition.isTimeToShow(now, TestHelper.createDeviceInfo(100), TestHelper.createDateMetaData(name, justBefore)));
        assertFalse(dateCondition.isTimeToShow(justBefore, TestHelper.createDeviceInfo(100), TestHelper.createDateMetaData(name, justBefore)));
        assertTrue(dateCondition.isTimeToShow(justAfter, TestHelper.createDeviceInfo(100), TestHelper.createDateMetaData(name, justBefore)));
    }

    public void testUpdateTimeToShow() throws ParseException {
        final String name = "test";
        final DateCondition dateCondition = new DateCondition(name, 12*3600);

        final Date now = TimeHelper.timeFromString("2016.02.15 10:32");

        final Calendar cal = Calendar.getInstance();
        cal.setTime(now);

        cal.add(Calendar.SECOND, 3600 * 12);
        final Date justBefore = cal.getTime();

        cal.add(Calendar.HOUR, 1);
        cal.add(Calendar.MINUTE, 30);
        final Date testTime = cal.getTime();

        final DeviceInfo deviceInfo = TestHelper.createDeviceInfo(100);
        final Map<String, String> metaData = TestHelper.createDateMetaData(name, justBefore);

        assertTrue(dateCondition.isTimeToShow(testTime, deviceInfo, metaData));
        dateCondition.updateNextTimeToShow(testTime, deviceInfo, metaData);
        assertEquals("2016.02.16 12:02", metaData.get("test.date"));
    }

}


