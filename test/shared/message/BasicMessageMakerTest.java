package shared.message;

import junit.framework.TestCase;
import shared.config.DeviceInfo;

import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;

public class BasicMessageMakerTest extends TestCase {


    DeviceInfo createDeviceInfo(int sessionId) {
        return new DeviceInfo("1.1.1.1", "abc-123", sessionId, "1.0", "test", "1.0", "testModel", "testDevice", "GMT+0", "en", "sparcnet", "1111111");
    }


    public void testBasics() {

        final AtomicBoolean wasUpdated1 = new AtomicBoolean(false);
        final AtomicBoolean wasUpdated2 = new AtomicBoolean(false);
        final AtomicBoolean wasUpdated3 = new AtomicBoolean(false);

        final MessageCondition trueCondition = new MessageCondition() {
            @Override
            public boolean isTimeToShow(Date now, DeviceInfo deviceInfo, Map<String, String> messageDataSet) { return true; }

            @Override
            public void updateNextTimeToShow(Date now, DeviceInfo deviceInfo, Map<String, String> messageDataSet) { wasUpdated1.set(true); }
        };
        final MessageCondition falseCondition = new MessageCondition() {
            @Override
            public boolean isTimeToShow(Date now, DeviceInfo deviceInfo, Map<String, String> messageDataSet) { return false; }

            @Override
            public void updateNextTimeToShow(Date now, DeviceInfo deviceInfo, Map<String, String> messageDataSet) { wasUpdated2.set(true); }
        };
        final MessageData messageData1 = new MessageData(MessageData.Location.ANY, "http://www.dn.se");
        final MessageData messageData2 = new MessageData(MessageData.Location.ANY, "http://www.svd.se");

        final MessageProvider messageProvider1 = new MessageProvider() {
            @Override
            public MessageData createMessage(DeviceInfo deviceInfo) {
                return messageData1;
            }
        };
        final MessageProvider messageProvider2 = new MessageProvider() {
            @Override
            public MessageData createMessage(DeviceInfo deviceInfo) {
                return messageData2;
            }
        };


        final List<MessageProviderConditionPair> providerConditionList = new ArrayList<MessageProviderConditionPair>();
        providerConditionList.add(new MessageProviderConditionPair(trueCondition, messageProvider1));
        providerConditionList.add(new MessageProviderConditionPair(falseCondition, messageProvider2));

        final BasicMessageMaker messageMaker = new BasicMessageMaker(providerConditionList, new MetaDataStore() {
            @Override
            public Map<String, String> getMetaData(String deviceId) {
                return metaData;
            }

            @Override
            public void putMetaData(String deviceId, Map<String, String> messageData) {
                metaData = messageData;
                wasUpdated3.set(true);
            }

            private Map<String, String> metaData = new HashMap<String, String>();
        });

        MessageData[] messageData = messageMaker.getLaunchMessages(createDeviceInfo(100));
        assertNotNull(messageData);
        assertEquals(1, messageData.length);

        assertTrue(wasUpdated1.get());
        assertFalse(wasUpdated2.get());
        assertTrue(wasUpdated3.get());
    }
}