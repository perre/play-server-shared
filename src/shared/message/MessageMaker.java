package shared.message;

import shared.config.DeviceInfo;

public interface MessageMaker {
    MessageData[] getLaunchMessages(DeviceInfo deviceInfo);
}
