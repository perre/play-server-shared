package shared.push;

import com.notnoop.apns.*;
import play.Logger;

public class PushService {

    public PushService(boolean useProductionServers, String certFile, String certPassword) {
        final ApnsDelegate delegate = new ApnsCallback(useProductionServers ? "Prod" : "Dev");

        ApnsServiceBuilder apnsServiceBuilder = APNS.newService().withDelegate(delegate).withCert(certFile, certPassword);
        if (useProductionServers) {
            apnsServiceBuilder = apnsServiceBuilder.withProductionDestination();
        } else {
            apnsServiceBuilder = apnsServiceBuilder.withSandboxDestination();
        }

        service = apnsServiceBuilder.build();
    }


    public void pushMessage(String token, String message) {
        service.push(token, APNS.newPayload().alertBody(message).build());
    }

    private class ApnsCallback implements ApnsDelegate {

        public ApnsCallback(String prefix) {

            this.prefix = prefix + ": ";
        }

        public void messageSent(final ApnsNotification message, final boolean resent) {
            Logger.info(prefix + "Sent message, resent: " + resent);
        }

        public void messageSendFailed(final ApnsNotification message, final Throwable e) {
            Logger.info(prefix + "Failed message " + message);
        }

        public void connectionClosed(final DeliveryError e, final int messageIdentifier) {
            Logger.info(prefix + "Closed connection: " + messageIdentifier + "\n   deliveryError " + e.toString());
        }

        public void cacheLengthExceeded(final int newCacheLength) {
            Logger.info(prefix + "cacheLengthExceeded " + newCacheLength);

        }

        public void notificationsResent(final int resendCount) {
            Logger.info(prefix + "notificationResent " + resendCount);
        }

        private final String prefix;
    }

    private final ApnsService service;
}
