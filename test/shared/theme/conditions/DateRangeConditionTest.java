package shared.theme.conditions;

import junit.framework.TestCase;
import shared.config.DeviceInfo;
import shared.theme.ThemeCondition;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class DateRangeConditionTest extends TestCase {

    public void testIsActiveGMT() throws Exception {
        ThemeCondition condition = new DateRangeCondition("2015.12.02", "2015.12.03");

        final DateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd hh:mm", Locale.ENGLISH);
        dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        final Date date1 = dateFormat.parse("2015.12.01 23:59");
        final Date date2 = dateFormat.parse("2015.12.02 00:00");
        final Date date3 = dateFormat.parse("2015.12.03 00:00");
        final Date date4 = dateFormat.parse("2015.12.03 00:01");

        final DeviceInfo deviceInfo = new DeviceInfo(null, null, 0, null, null, null, null, null, "GMT", null, null, null);

        assertFalse(condition.isActive(date1, deviceInfo));
        assertTrue(condition.isActive(date2, deviceInfo));
        assertTrue(condition.isActive(date3, deviceInfo));
        assertFalse(condition.isActive(date4, deviceInfo));

    }

    public void testIsActiveSweden() throws Exception {
        ThemeCondition condition = new DateRangeCondition("2015.12.02", "2015.12.03");

        final DateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd hh:mm", Locale.ENGLISH);
        dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        final Date date1 = dateFormat.parse("2015.12.01 22:59");
        final Date date2 = dateFormat.parse("2015.12.02 23:00");
        final Date date3 = dateFormat.parse("2015.12.02 23:00");
        final Date date4 = dateFormat.parse("2015.12.02 23:01");

        final DeviceInfo deviceInfo = new DeviceInfo(null, null, 0, null, null, null, null, null, "GMT+1", null, null, null);

        assertFalse(condition.isActive(date1, deviceInfo));
        assertTrue(condition.isActive(date2, deviceInfo));
        assertTrue(condition.isActive(date3, deviceInfo));
        assertFalse(condition.isActive(date4, deviceInfo));

    }

    public void testTimeZonesFromTheWild() {
        final String[] timeZones = new String[] {
                "ADT",
                "AEDT",
                "AEST",
                "AKST",
                "AST",
                "BRST",
                "BST",
                "CDT",
                "CEST",
                "CET",
                "CST",
                "EDT",
                "EEST",
                "EET",
                "EST",
                "GMT",
                "GMT+1",
                "GMT+10",
                "GMT+2",
                "GMT+3",
                "GMT+4",
                "GMT+4:30",
                "GMT+5",
                "GMT+5:30",
                "GMT+6",
                "GMT+7",
                "GMT+8",
                "GMT+9",
                "GMT-4",
                "GMT-5",
                "GMT-6",
                "GMT-7",
                "GMT-8",
                "GMT−4",
                "HST",
                "IST",
                "JST",
                "MDT",
                "MESZ",
                "MEZ",
                "MST",
                "PDT",
                "PST",
                "SAST",
                "SEČ",
                "UTC",
                "UTC+1",
                "UTC+2",
                "UTC−10",
                "UTC−4",
                "UTC−5",
                "WITA",
        };

        java.util.List<String> toFix = new java.util.LinkedList<String>();
        for (String timeZone : timeZones) {
            final String altTimeZone = TimeZoneConverter.convert(timeZone);
            final TimeZone tz = TimeZone.getTimeZone(altTimeZone);
            String id = tz.getID();
            if (!id.startsWith("GMT")) {
                toFix.add(altTimeZone);
            }
            assertNotNull(tz);
        }


    }
}