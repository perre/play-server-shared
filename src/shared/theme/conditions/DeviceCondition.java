package shared.theme.conditions;

import shared.config.DeviceInfo;
import shared.theme.ThemeCondition;

import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DeviceCondition implements ThemeCondition {

    public DeviceCondition(String regexp, DeviceInfoSelector selector) {
        this.pattern =  Pattern.compile(regexp);;
        this.selector = selector;
    }

    @Override
    public boolean isActive(Date now, DeviceInfo deviceInfo) {
        String value = selector.select(deviceInfo);
        if (value != null) {
            final Matcher matcher = pattern.matcher(value);
            return matcher.find();
        } else {
            return false;
        }
    }

    private final Pattern pattern;
    private final DeviceInfoSelector selector;
}
