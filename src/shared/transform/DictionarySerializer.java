package shared.transform;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.Map;

public class DictionarySerializer {

    public DictionarySerializer(byte[] key) {
        this.key = key.clone();
    }

    public String Serialize(Map<String, String> dict) throws Exception {
        final byte[] jsonBlob = mapper.writeValueAsString(dict).getBytes();
        return bytesToHexString(jsonBlob);
    }


    public Map<String, String> Deserialize(String encodedString) throws Exception {
        final byte[] blob = hexStringToBytes(encodedString);
        return mapper.readValue(blob, storedTypeGameData);
    }

    private byte[] hexStringToBytes(String hexString) throws Exception {
        byte[] result = new byte[hexString.length() / 2];

        for (int ii = 0; ii < result.length ; ii++) {
            int v16 = hexCharToByte(hexString.charAt(ii*2));
            int v1 = hexCharToByte(hexString.charAt(ii*2+1));
            int value = (v16 << 4) + v1;
            value ^= key[ii % key.length];
            result[ii] = (byte) value;
        }

        return result;
    }

    private int hexCharToByte(char ch) throws Exception {
        if (ch >= '0' && ch <= '9') {
            return ch - '0';
        } else if (ch >= 'a' && ch <= 'f') {
            return ch - 'a' + 10;
        } else if (ch >= 'A' && ch <= 'F') {
            return ch - 'A' + 10;
        }
        throw new Exception("Invalid hex-char '" + ch + "'");
    }

    private String bytesToHexString(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for ( int ii = 0; ii < bytes.length; ii++ ) {
            int v = bytes[ii] & 0xFF;
            v ^= key[ii % key.length];
            hexChars[ii * 2] = hexAlphabet[v >>> 4];
            hexChars[ii * 2 + 1] = hexAlphabet[v & 0x0F];
        }
        return new String(hexChars);
    }

    private final static char[] hexAlphabet = "0123456789abcdef".toCharArray();

    private final byte[] key;

    private final ObjectMapper mapper = new ObjectMapper();
    private final JavaType storedTypeGameData = mapper.getTypeFactory().
            constructMapType(Map.class, String.class, String.class);
}



// Dict -> json -> blob -> string