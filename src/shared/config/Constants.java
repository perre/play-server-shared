package shared.config;

class Constants {

    static final String DEVICE_ID = "uid";
    static final String SESSION_ID = "sid";
    //static final String VERSION = "version";
    static final String BUILD = "build";
    static final String SYSTEM_NAME = "sysname";
    static final String SYSTEM_VERSION = "sysver";
    static final String DEVICE_MODEL = "model";
    static final String DEVICE_NAME = "device";
    static final String TIME_ZONE = "tz";
    static final String TIME_ZONE2 = "tz2";
    static final String LANGUAGE = "lang";
    static final String SSID = "ssid";
    static final String BSSID = "bssid";
    static final String METADATA = "meta";
    static final String SOUND = "sound";
}
