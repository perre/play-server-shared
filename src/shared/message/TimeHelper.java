package shared.message;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TimeHelper {

    public static String timeToString(Date date) {
        return dateFormat.format(date);
    }


    public static Date timeFromString(String str) throws ParseException {
        return dateFormat.parse(str);
    }

    static final DateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd HH:mm");

}
