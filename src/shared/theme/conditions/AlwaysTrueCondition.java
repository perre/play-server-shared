package shared.theme.conditions;

import shared.config.DeviceInfo;
import shared.theme.ThemeCondition;

import java.util.Date;

public class AlwaysTrueCondition implements ThemeCondition {

    @Override
    public boolean isActive(Date now, DeviceInfo deviceInfo) {
        return true;
    }
}
