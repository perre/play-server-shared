package shared.theme;

import shared.config.DeviceInfo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DefaultThemeRegistry<THEME> implements ThemeRegistry<THEME> {

    public void addTheme(ThemeCondition themeCondition, THEME theme) {
        themes.add(new ThemeConditionPair<THEME>(themeCondition, theme));
    }

    @Override
    public THEME getActiveTheme(Date now, DeviceInfo deviceInfo) {
        for (final ThemeConditionPair<THEME> themeConditionPair : themes) {
            if (themeConditionPair.themeCondition.isActive(now, deviceInfo))
                return themeConditionPair.theme;
        }
        return null;
    }

    private final List<ThemeConditionPair<THEME>> themes = new ArrayList<ThemeConditionPair<THEME>>();
}
