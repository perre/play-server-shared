package shared.config;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import play.Logger;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.Results;
import shared.playhelper.RequestParser;
import shared.transform.DictionarySerializer;

import java.util.HashMap;
import java.util.Map;

public class ConfigHelper {

    public ConfigHelper(ConfigDataSource dataSource) {
        this.dataSource = dataSource;
    }

    public Result process(Http.Request request) throws JsonProcessingException {
        final Http.RequestBody body = request.body();
        String ip = RequestParser.ipFromRequest(request);
        if (body != null) {
            final JsonNode json = body.asJson();
            if (json != null) {

                final String deviceId = stringFromProperty(json, null, Constants.DEVICE_ID);
                final String metaData = stringFromProperty(json, null, Constants.METADATA);
                final int sessionId = intFromProperty(json, Constants.SESSION_ID);

                Map<String, String> metaDataDict = null;
                if (metaData != null) {
                    try {
                        final DictionarySerializer dictionarySerializer = new DictionarySerializer(dataSource.getMetaKey(deviceId));
                        metaDataDict = dictionarySerializer.Deserialize(metaData);
                    } catch (Exception e) {
                        Logger.warn("Can't parse meta data from device " + deviceId + ", " + metaData);
                    }
                }

                final String version = stringFromProperty(json, metaDataDict, Constants.BUILD);
                final String systemName = stringFromProperty(json, metaDataDict, Constants.SYSTEM_NAME);
                final String systemVersion = stringFromProperty(json, metaDataDict, Constants.SYSTEM_VERSION);
                final String deviceModel = stringFromProperty(json, metaDataDict, Constants.DEVICE_MODEL);
                final String timeZone = stringFromProperty(json, metaDataDict, Constants.TIME_ZONE);          // Original sloppy time zone
                final String timeZoneSafe = stringFromProperty(json, metaDataDict, Constants.TIME_ZONE2);     // Safer format GMT[+/-]<hours>
                final String language = stringFromProperty(json, metaDataDict, Constants.LANGUAGE);

                final String deviceName = stringFromProperty(json, metaDataDict, Constants.DEVICE_NAME);
                final String SSID = stringFromProperty(json, metaDataDict, Constants.SSID);
                final String BSSID = stringFromProperty(json, metaDataDict, Constants.BSSID);
                final String sound = stringFromProperty(json, metaDataDict, Constants.SOUND);


                if (deviceId != null) {
                    final DeviceInfo deviceInfo = new DeviceInfo(ip, deviceId, sessionId, version, systemName,
                            systemVersion, deviceModel, deviceName, timeZoneSafe != null ? timeZoneSafe : timeZone, language, SSID, BSSID, sound);

                    final Map<String, Object> config = getBasicConfig(deviceInfo);
                    this.dataSource.updateConfig(config, deviceInfo, json);

                    final String jsonResponse = mapper.writeValueAsString(config);

                    Logger.info("Configured device " + deviceId + "/" + ip + " : " + json.toString() + " => " + jsonResponse);

                    return Results.ok(jsonResponse);
                }
            }
        }
        return Results.badRequest();
    }

    private String stringFromProperty(JsonNode json, Map<String, String> metaDataDict, String propertyName) {
        final JsonNode node = json.get(propertyName);
        String reply = node != null ? node.asText() : null;
        if (reply == null && metaDataDict != null) {
            reply = metaDataDict.get(propertyName);
        }

        return reply;
    }

    private int intFromProperty(JsonNode json, String propertyName) {
        final JsonNode node = json.get(propertyName);
        return node != null ? node.asInt() : 0;
    }

    private Map<String, Object> getBasicConfig(DeviceInfo deviceInfo) {
        final Map<String, Object> config = new HashMap<String, Object>();
        config.put("tr", Boolean.valueOf(this.dataSource.doTrack(deviceInfo)).toString());
        config.put("pu", Boolean.valueOf(this.dataSource.doPush(deviceInfo)).toString());
        return config;
    }

    private final ConfigDataSource dataSource;

    private static final ObjectMapper mapper = new ObjectMapper();
}
