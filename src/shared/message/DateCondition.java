package shared.message;

import shared.config.DeviceInfo;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

public class DateCondition implements MessageCondition {


    public DateCondition(String prefix, int deltaTimeSeconds) {
        this.prefix = prefix;
        this.deltaTimeSeconds = deltaTimeSeconds;
    }


    @Override
    public boolean isTimeToShow(Date now, DeviceInfo deviceInfo, Map<String, String> messageDataSet) {
        Date nextTime = null;
        try {
            final String nextTimeString = messageDataSet.get(getKey(NEXT_DATE));
            if (nextTimeString != null) {
                 nextTime = TimeHelper.timeFromString(nextTimeString);
            }
        } catch (ParseException e) {
        }
        return nextTime == null || nextTime.before(now);
    }

    @Override
    public void updateNextTimeToShow(Date now, DeviceInfo deviceInfo, Map<String, String> messageDataSet) {
        final Calendar cal = Calendar.getInstance();
        cal.setTime(now);
        cal.add(Calendar.SECOND, deltaTimeSeconds);
        final Date nextTime = cal.getTime();
        messageDataSet.put(getKey(NEXT_DATE), TimeHelper.timeToString(nextTime));
    }

    private String getKey(String suffix) {
        return prefix + "." + suffix;
    }

    private final String prefix;
    private final int deltaTimeSeconds;

    private static final String NEXT_DATE = "date";
}


