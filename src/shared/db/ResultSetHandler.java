package shared.db;

import java.sql.ResultSet;
import java.sql.SQLException;

public interface ResultSetHandler {

    boolean handleResultSet(ResultSet resultSet) throws SQLException;
}
