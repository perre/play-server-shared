package shared.theme;

import shared.config.DeviceInfo;

import java.util.Date;

public interface ThemeCondition {
    boolean isActive(Date now, DeviceInfo deviceInfo);
}
