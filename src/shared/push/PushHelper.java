package shared.push;

import com.fasterxml.jackson.databind.JsonNode;

import play.mvc.Http;
import play.mvc.Result;
import play.mvc.Results;

import shared.playhelper.RequestParser;

public class PushHelper {

    public PushHelper(PushDelegate delegate, PushService prodPushService, PushService devPushService) {

        this.delegate = delegate;
        this.prodPushService = prodPushService;
        this.devPushService = devPushService;
    }

    public Result process(Http.Request request) {
        boolean ok = false;
        String ip = RequestParser.ipFromRequest(request);
        if (this.delegate.isValidSource(ip)) {
            final Http.RequestBody body = request.body();
            if (body != null) {
                final JsonNode req = body.asJson();

                final JsonNode messageJSON = req.get(MESSAGE);
                if (messageJSON == null)
                    return Results.badRequest();

                final JsonNode tokensJSONs = req.get(TOKENS);
                if (tokensJSONs == null)
                    return Results.badRequest();

                boolean isProductionEnv = true;
                final JsonNode envJSON = req.get(ENV);
                if (envJSON != null) {
                    if (envJSON.asText().equals("dev")) {
                        isProductionEnv = false;
                    }
                }

                final String message = messageJSON.asText();

                for (int ii = 0; ii < tokensJSONs.size(); ++ii) {
                    final JsonNode tokenJSON = tokensJSONs.get(ii);
                    (isProductionEnv ? prodPushService : devPushService).pushMessage(tokenJSON.asText(), message);
                }

                ok = true;

            }
        }
        return ok ? Results.noContent() : Results.badRequest();
    }

    private final PushDelegate delegate;
    private final PushService prodPushService;
    private final PushService devPushService;

    private static final String TOKENS = "tokens";
    private static final String MESSAGE = "message";
    private static final String ENV = "env";
}
