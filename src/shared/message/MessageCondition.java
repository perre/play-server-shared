package shared.message;

import shared.config.DeviceInfo;

import java.util.Date;
import java.util.Map;

public interface MessageCondition {

    boolean isTimeToShow(Date now, DeviceInfo deviceInfo, Map<String, String> messageDataSet);

    void updateNextTimeToShow(Date now, DeviceInfo deviceInfo, Map<String, String> messageDataSet);
}
