package shared.theme;

import junit.framework.TestCase;
import shared.config.DeviceInfo;

import java.util.Date;

public class DefaultThemeRegistryTest extends TestCase {

    private interface TestTheme {
        String name();
    }

    public void testGetActiveTheme() throws Exception {
        DefaultThemeRegistry<TestTheme> registry = new DefaultThemeRegistry<TestTheme>();
        registry.addTheme(new ThemeCondition() {
            @Override
            public boolean isActive(Date now, DeviceInfo deviceInfo) {
                return false;
            }
        }, new TestTheme() {
            @Override
            public String name() {
                return "1";
            }
        });
        registry.addTheme(new ThemeCondition() {
            @Override
            public boolean isActive(Date now, DeviceInfo deviceInfo) {
                return true;
            }
        }, new TestTheme() {
            @Override
            public String name() {
                return "2";
            }
       });

        TestTheme theme = registry.getActiveTheme(new Date(), null);
        assertEquals("2", theme.name());
    }
}