package shared.db;

import play.Logger;

import java.net.URI;
import java.net.URISyntaxException;
import java.sql.*;

public class DbHelper {

    public static Connection getConnection() throws URISyntaxException, SQLException {
        final String databaseUrl = System.getenv("DATABASE_URL");
        URI dbUri;
        dbUri = new URI(databaseUrl);
        final String[] parts = dbUri.getUserInfo().split(":");
        final String username = parts[0];
        final String password = parts.length > 1 ? parts[1] : "";
        final String dbUrl = "jdbc:postgresql://" + dbUri.getHost() + dbUri.getPath();

        return DriverManager.getConnection(dbUrl, username, password);
    }

    public static int executeStatement(final StatementBuilder statementBuilder) {
        int changes = -1;
        PreparedStatement stmt = null;
        try {
            stmt = statementBuilder.getStatement();
            changes = stmt.executeUpdate();
        } catch (final SQLException e) {
            Logger.error(e.getMessage());
        } finally {
            try {
                if (stmt != null)
                    stmt.close();
            } catch (final SQLException e) {
                Logger.error(e.getMessage());
            }
        }
        return changes;
    }

    public static boolean executeQuery(final StatementBuilder statementBuilder, ResultSetHandler resultSetHandler) {
        boolean isOk = false;
        PreparedStatement stmt = null;
        try {
            stmt = statementBuilder.getStatement();
            ResultSet resultSet = stmt.executeQuery();
            isOk = resultSetHandler.handleResultSet(resultSet);
            resultSet.close();
        } catch (final SQLException e) {
            Logger.error(e.getMessage());
        } finally {
            try {
                if (stmt != null)
                    stmt.close();
            } catch (final SQLException e) {
                Logger.error(e.getMessage());
            }
        }
        return isOk;
    }

}
