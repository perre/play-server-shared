package shared.message;

import shared.config.DeviceInfo;

import java.util.Date;
import java.util.Map;

public class SessionCondition implements MessageCondition {


    public SessionCondition(String prefix, int sessionIdThreshold, int deltaSessionId) {
        this.prefix = prefix;
        this.sessionIdThreshold = sessionIdThreshold;
        this.deltaSessionId = deltaSessionId;
    }


    @Override
    public boolean isTimeToShow(Date now, DeviceInfo deviceInfo, Map<String, String> messageDataSet) {
        final String strValue = messageDataSet.get(getKey(NEXT_SESSION_ID));
        final int value = strValue != null ? Integer.valueOf(strValue) : 0;
        return deviceInfo.sessionId >= value && deviceInfo.sessionId > sessionIdThreshold;
    }

    @Override
    public void updateNextTimeToShow(Date now, DeviceInfo deviceInfo, Map<String, String> messageDataSet) {
        final int value = deviceInfo.sessionId + deltaSessionId;
        messageDataSet.put(getKey(NEXT_SESSION_ID), "" + value);
    }

    private String getKey(String suffix) {
        return prefix + "." + suffix;
    }

    private final String prefix;
    private final int sessionIdThreshold;
    private final int deltaSessionId;

    private static final String NEXT_SESSION_ID = "sessionid";
}
