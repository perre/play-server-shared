package shared.message;

import shared.config.DeviceInfo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class CompositeCondition implements MessageCondition {

    public CompositeCondition(MessageCondition... inConditions) {
        for (MessageCondition condition : inConditions) {
            conditions.add(condition);
        }
    }

    @Override
    public boolean isTimeToShow(Date now, DeviceInfo deviceInfo, Map<String, String> messageDataSet) {
        boolean isTime = false;
        for (MessageCondition condition : conditions) {
            isTime |= condition.isTimeToShow(now, deviceInfo, messageDataSet);
        }
        return isTime;
    }

    @Override
    public void updateNextTimeToShow(Date now, DeviceInfo deviceInfo, Map<String, String> messageDataSet) {
        for (MessageCondition condition : conditions) {
            condition.updateNextTimeToShow(now, deviceInfo, messageDataSet);
        }
    }

    private List<MessageCondition> conditions = new ArrayList<MessageCondition>();
}
