package shared.message;

// This class is serialized for the client, you can't rename stuff here without breaking the client!
public class MessageData {

    public enum Location {
        PRE_GAME,
        POST_GAME,
        ANY,
    }

    public MessageData(MessageData.Location location, String url) {
        this.loc = location;
        this.url = url;
    }

    public Location getLoc() {
        return loc;
    }

    public String getUrl() {
        return url;
    }

    private final Location loc;     // Location in game
    private final String url;       // URL to display

}
