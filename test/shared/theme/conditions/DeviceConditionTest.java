package shared.theme.conditions;

import junit.framework.TestCase;
import shared.config.DeviceInfo;
import shared.theme.ThemeCondition;

import java.util.Date;

public class DeviceConditionTest extends TestCase {

    public void testBasics() throws Exception {
        final DeviceInfo deviceInfoShort = makeDeviceInfo("sparcnet");
        final DeviceInfo deviceInfoLong = makeDeviceInfo("sparcnet 5GHz");

        {
            final String pattern = "^sparcnet(.)*";
            assertTrue(matchesPattern(pattern, deviceInfoShort));
            assertTrue(matchesPattern(pattern, deviceInfoLong));
        }

        {
            final String pattern = "sparcnet(.)*";
            assertTrue(matchesPattern(pattern, deviceInfoShort));
            assertTrue(matchesPattern(pattern, deviceInfoLong));
        }

        {
            final String pattern = "sparcnet 5GHz";
            assertFalse(matchesPattern(pattern, deviceInfoShort));
            assertTrue(matchesPattern(pattern, deviceInfoLong));
        }

        {
            final String pattern = "sparcnet";
            assertTrue(matchesPattern(pattern, deviceInfoShort));
            assertTrue(matchesPattern(pattern, deviceInfoLong));
        }

        {
            final String pattern = "sparcnet$";
            assertTrue(matchesPattern(pattern, deviceInfoShort));
            assertFalse(matchesPattern(pattern, deviceInfoLong));
        }

        {
            final String pattern = "sparcnet2";
            assertFalse(matchesPattern(pattern, deviceInfoShort));
            assertFalse(matchesPattern(pattern, deviceInfoLong));
        }

        {
            final String pattern = "sharknado";
            assertFalse(matchesPattern(pattern, deviceInfoShort));
            assertFalse(matchesPattern(pattern, deviceInfoLong));
        }

        {
            final String pattern = "(.)*sparc(.)*";
            assertTrue(matchesPattern(pattern, deviceInfoShort));
            assertTrue(matchesPattern(pattern, deviceInfoLong));
        }

        {
            final String pattern = " sparcnet";
            assertFalse(matchesPattern(pattern, deviceInfoShort));
            assertFalse(matchesPattern(pattern, deviceInfoLong));
        }
    }

    private DeviceInfo makeDeviceInfo(String ssid) {
        return new DeviceInfo("ip", "id", 0, "version", "sysname", "sysver", "model", "devicename", "GMT", "en", ssid, "00:11:22");
    }

    private boolean matchesPattern(String pattern, DeviceInfo deviceInfo) {
        ThemeCondition condition = new DeviceCondition(pattern, new DeviceInfoSelector() {
            @Override
            public String select(DeviceInfo deviceInfo) {
                return deviceInfo.SSID;
            }
        });
        return condition.isActive(new Date(), deviceInfo);
    }

}