package shared.transform;

import junit.framework.TestCase;

import java.util.HashMap;
import java.util.Map;

public class DictionarySerializerTest extends TestCase {

    public void testEncoding() throws Exception {
        Map<String, String> in = createTestMap();

        String out = ds.Serialize(in);
        assertEquals(result, out);
    }

    public void testDecoding() throws Exception {
        Map<String, String> dict = ds.Deserialize(result);
        assertEquals(createTestMap(), dict);
    }

    private Map<String, String> createTestMap() {
        Map<String, String> in = new HashMap<String, String>();
        in.put("device", "pPhone+");
        in.put("ssid", "sparcnet 5GHz");
        in.put("lang", "sv-SE");
        return in;
    }

    private final DictionarySerializer ds = new DictionarySerializer("abc".getBytes());
    private final String result = "1a400704140a0207415b4013310a0c0f0748434e4112110a0540594311130010000f07174157242918414d400f000c0443584112144e3227411c";
}