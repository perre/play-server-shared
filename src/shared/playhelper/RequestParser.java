package shared.playhelper;

public class RequestParser {

    private static String filterIpList(String ipList) {
        String reply = null;
        if (ipList != null) {
            reply = ipList;
            String[] parts = ipList.split(",");
            if (parts.length > 1) {
                reply = parts[parts.length-1].trim();
            }
        }
        return reply;
    }

    public static String ipFromRequest(play.mvc.Http.Request request) {
        final String ip = request.remoteAddress();
        final String ip2 = request.getHeader("X-Forwarded-For");
        return  filterIpList(ip2 != null ? ip2 : ip);
    }
}
