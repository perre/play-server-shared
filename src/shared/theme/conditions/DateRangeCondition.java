package shared.theme.conditions;

import shared.config.DeviceInfo;
import shared.theme.ThemeCondition;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class DateRangeCondition implements ThemeCondition {

    public DateRangeCondition(String startDateString, String endDateString) {
        this.startDateString = startDateString;
        this.endDateString = endDateString;
        getDateFormat("GMT");
    }

    private DateFormat getDateFormat(String timeZone) {
        final DateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd", Locale.ENGLISH);
        dateFormat.setTimeZone(TimeZone.getTimeZone(timeZone));
        return dateFormat;
    }

    @Override
    public boolean isActive(Date now, DeviceInfo deviceInfo) {
        boolean result = false;
        final DateFormat dateFormat = getDateFormat(TimeZoneConverter.convert(deviceInfo.timeZone));
        try {
            final Date startDate = dateFormat.parse(startDateString);
            final Date stopDate = dateFormat.parse(endDateString);
            final boolean tooSoon = now.before(startDate);
            final boolean tooLate = now.after(stopDate);
            result = !tooSoon && !tooLate;
        } catch (ParseException e) {
        }
        return result;
    }

    private final String startDateString;
    private final String endDateString;


}
