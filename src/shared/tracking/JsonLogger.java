package shared.tracking;

import com.fasterxml.jackson.databind.JsonNode;
import play.Logger;

import java.util.Map;

public class JsonLogger implements JsonHandler {

    @Override
    public void handle(String ip, JsonNode json, Map<String, Object> config) {
        Logger.info(ip + ":" + json.toString());
    }
}

