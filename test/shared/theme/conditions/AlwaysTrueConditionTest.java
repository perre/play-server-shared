package shared.theme.conditions;

import junit.framework.TestCase;
import shared.theme.ThemeCondition;

import java.util.Date;

public class AlwaysTrueConditionTest extends TestCase {

    public void testBasics() throws Exception {
        ThemeCondition themeCondition = new AlwaysTrueCondition();
        assertEquals(true, themeCondition.isActive(new Date(), null));
    }
}