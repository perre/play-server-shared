package shared.message;

import junit.framework.TestCase;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

public class TimeHelperTest extends TestCase {

    public void testFixedTime() throws ParseException {
        final String test = "2016.02.15 14:32";
        final Date date = TimeHelper.timeFromString(test);
        final String out = TimeHelper.timeToString(date);
        assertEquals(test, out);
    }

    public void testCurrentTime() throws ParseException {
        final Date now = Calendar.getInstance().getTime();
        final String nowString = TimeHelper.timeToString(now);
        final Date restoredDate = TimeHelper.timeFromString(nowString);
        final String nowString2 = TimeHelper.timeToString(restoredDate);
        assertEquals(nowString, nowString2);
    }

}